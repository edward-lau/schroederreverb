# Schroeder Reverb

A reverb plugin programmed in C++ using the JUCE framework.
The bulk of the DSP code can be found in SchroederReverb.cpp, AllPassFilter.cpp and CombFilter.cpp .
