/*
  ==============================================================================

    SchroederReverb.cpp
    Created: 7 Dec 2017 2:33:34pm
    Author:  Edward Lau

  ==============================================================================
*/

#include "SchroederReverb.h"

SchroederReverb::SchroederReverb ()
{
    const float maxDelayTime = 0.1f;

    // Initialize comb filters
    for (int i = 0; i < numCombs; i++)
    {
        float gain = calculateCombFilterGain (m_combDelayTimes[i], m_parameters.decay);
        m_combFilters[i].initialize (m_combDelayTimes[i], maxDelayTime, gain);
    }

    // Initialize allpass filters
    for (int i = 0; i < numAllPasses; i++)
    {
        m_allPassFilters[i].initialize (m_allPassDelayTimes[i], maxDelayTime, m_allPassFilterGains[i]);
    }
}

SchroederReverb::~SchroederReverb()
{

}

void SchroederReverb::setParameters (const Parameters& parameters)
{
    m_mix.setValue (parameters.mix);

    for (int i = 0; i < numCombs; i++)
    {
        float gain = calculateCombFilterGain (m_combDelayTimes[i], parameters.decay);
        m_combGains[i].setValue (gain);
    }

    m_parameters = parameters;
}

void SchroederReverb::setSampleRate (double sampleRate)
{
    for (int i = 0; i < numCombs; i++)
    {
        m_combFilters[i].setSampleRate (sampleRate);
    }

    for (int i = 0; i < numAllPasses; i++)
    {
        m_allPassFilters[i].setSampleRate (sampleRate); 
    }

    // Reset LinearSmoothedValue objects and parameters
    const int smoothTime = 0.1;
    m_mix.reset   (sampleRate, smoothTime);
    setParameters (m_parameters);
}

void SchroederReverb::processBlock (AudioSampleBuffer& buffer, int totalNumInputChannels, int totalNumOutputChannels)
{
    const int numSamples = buffer.getNumSamples();
    float** pChannelData = buffer.getArrayOfWritePointers();
    float summingGain = 1.0f / totalNumInputChannels;

    for (int sample = 0; sample < numSamples; sample++)
    {
        // Input summing
        float sumInput = 0.0;
        for (int inChannel = 0; inChannel < totalNumInputChannels; inChannel++)
        {
            sumInput += pChannelData[inChannel][sample];
        }
        sumInput *= summingGain;

        float output = 0.0;

        // Process parallel comb filters
        for (int i = 0; i < numCombs; i++)
        {
            const float gain = m_combGains[i].getNextValue();
            output += m_combFilters[i].process(sumInput * 0.25f, gain);
        }

        // Processs series allpass filters
        for (int i = 0; i < numAllPasses; i++)
        {
            output = m_allPassFilters[i].process(output);
        }

        // Output and scaling
        const float mixValue = m_mix.getNextValue();
        int stereoChannel = 0;
        for (int outChannel = 0; outChannel < totalNumOutputChannels; outChannel++)
        {
            pChannelData[stereoChannel][sample] *= (1 - mixValue);
            pChannelData[stereoChannel][sample] += output * mixValue;
            stereoChannel = (outChannel < 1 ? outChannel + 1 : 0);
        }
    }
}

float SchroederReverb::calculateCombFilterGain (float delayTime, float totalReverbTime)
{
    return powf (0.001f, delayTime / totalReverbTime);
}