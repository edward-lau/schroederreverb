/*
  ==============================================================================

    AllPassFilter.h
    Created: 7 Dec 2017 2:33:48pm
    Author:  Edward Lau

  ==============================================================================
*/

#ifndef ALLPASSFILTER_H_
#define ALLPASSFILTER_H_

class AllPassFilter
{
public:
    AllPassFilter();
    ~AllPassFilter();

    void initialize (float delayTime, float maxDelayTime, float gain);
    void setSampleRate (double sampleRate);
    float process (float sample);

private:
    AudioSampleBuffer m_buffer;
    float* m_pBuffer;
    float m_gain;
    float m_sampleRate;
    float m_delayTime, m_maxDelayTime;
    int m_delayTimeSamples;
    int m_bufferIndex = 0;
};

#endif // ALLPASSFILTER_H_
