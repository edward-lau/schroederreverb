/*
  ==============================================================================

    CombFilter.cpp
    Created: 7 Dec 2017 2:33:42pm
    Author:  Edward Lau

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "CombFilter.h"

CombFilter::CombFilter()
{
    m_buffer = AudioSampleBuffer (1, 441);
    m_buffer.clear();
    m_pBuffer = m_buffer.getWritePointer (0);
}

CombFilter::~CombFilter()
{
}

void CombFilter::initialize (float delayTime, float maxDelayTime, float gain)
{
    m_delayTime = delayTime;
    m_maxDelayTime = maxDelayTime;
    m_gain = gain;
}

void CombFilter::setSampleRate (double sampleRate)
{
    m_sampleRate = sampleRate;
    const int maxDelaySamples = (int)(m_maxDelayTime * sampleRate);
    if (maxDelaySamples != m_buffer.getNumSamples())
    {
        m_buffer.setSize (1, maxDelaySamples);
        m_buffer.clear();
        m_pBuffer = m_buffer.getWritePointer(0);
        m_bufferIndex = 0;
    }

    m_delayTimeSamples = (int)(m_delayTime * sampleRate);
}

float CombFilter::process (float sample, float gain)
{
    const float output = m_pBuffer[m_bufferIndex];
    m_pBuffer[m_bufferIndex] = sample + output * gain;
    m_bufferIndex = (m_bufferIndex != m_delayTimeSamples - 1 ? m_bufferIndex + 1 : 0);

    return output;
}