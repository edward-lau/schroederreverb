/*
  ==============================================================================

    SchroederReverb.h
    Created: 7 Dec 2017 2:33:34pm
    Author:  Edward Lau

  ==============================================================================
*/

#ifndef SCHROEDERREVERB_H_
#define SCHROEDERREVERB_H_

#include "../JuceLibraryCode/JuceHeader.h"
#include "CombFilter.h"
#include "AllPassFilter.h"

class SchroederReverb
{
public:
    SchroederReverb ();
    ~SchroederReverb();
    
    struct Parameters
    {
        Parameters()
        : mix (0.5),
        decay (1.0)
        {}

        float mix;
        float decay;
    };

    const Parameters& getParameters() { return m_parameters; };
    void setParameters (const Parameters& parameters);
    void setSampleRate (double sampleRate);
    void processBlock (AudioSampleBuffer& buffer, int totalNumInputChannels, int totalNumOutputChannels);

private:
    static const int numCombs = 4, numAllPasses = 2;
    float m_combDelayTimes[numCombs]         = { 0.0297f, 0.0371f, 0.0411f, 0.0437f };
    float m_allPassDelayTimes[numAllPasses]  = { 0.0050f, 0.0017f };
    float m_allPassFilterGains[numAllPasses] = { 0.707f, 0.5f };

    CombFilter m_combFilters[numCombs];
    AllPassFilter m_allPassFilters[numAllPasses];
    Parameters m_parameters;
    LinearSmoothedValue<float> m_mix;
    LinearSmoothedValue<float> m_combGains[numCombs];

    float calculateCombFilterGain (float delayTime, float totalReverbTime);
};

#endif // SCHROEDERREVERB_H_