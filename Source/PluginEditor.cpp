/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
SchroederReverbAudioProcessorEditor::SchroederReverbAudioProcessorEditor (SchroederReverbAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    addAndMakeVisible (mixLabel);
    mixLabel.setText ("Mix", dontSendNotification);

    addAndMakeVisible (mixSlider);
    mixSlider.setRange (0.0, 1.0);
    mixSlider.setValue (*processor.mixParam, dontSendNotification);
    mixSlider.addListener (this); 

    addAndMakeVisible (decayLabel);
    decayLabel.setText ("Decay", dontSendNotification);

    addAndMakeVisible (decaySlider);
    decaySlider.setRange (0.01, 3.0);
    decaySlider.setValue (*processor.decayParam, dontSendNotification);
    decaySlider.addListener (this);

    setSize (400, 80);
    startTimer (42);
}

SchroederReverbAudioProcessorEditor::~SchroederReverbAudioProcessorEditor()
{
}

//==============================================================================
void SchroederReverbAudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void SchroederReverbAudioProcessorEditor::resized()
{
    mixLabel.setBounds (10, 10, 90, 20);
    mixSlider.setBounds (100, 10, getWidth() - 110, 20);

    decayLabel.setBounds(10, 40, 90, 20);
    decaySlider.setBounds (100, 40, getWidth() - 110, 20);
}

void SchroederReverbAudioProcessorEditor::sliderValueChanged(Slider* slider)
{
    if (slider == &mixSlider)
    {
        const float mix = mixSlider.getValue();
        processor.setMix (mix);
    }

    if (slider == &decaySlider)
    {
        const float decay = decaySlider.getValue();
        processor.setDecay (decay);
    }
}

void SchroederReverbAudioProcessorEditor::timerCallback()
{
    mixSlider.setValue (*processor.mixParam, dontSendNotification);
    decaySlider.setValue (*processor.decayParam, dontSendNotification);
}