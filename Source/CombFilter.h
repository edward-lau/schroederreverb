/*
  ==============================================================================

    CombFilter.h
    Created: 7 Dec 2017 2:33:42pm
    Author:  Edward Lau

  ==============================================================================
*/

#ifndef COMBFILTER_H_
#define COMBFILTER_H_

class CombFilter
{
public:
    CombFilter();
    ~CombFilter();

    void initialize (float delayTime, float maxDelayTime, float gain);
    void setSampleRate (double sampleRate);
    float getDelayTime() { return m_delayTime; }
    float process (float sample, float gain);

private:
    AudioSampleBuffer m_buffer;
    float* m_pBuffer;
    float m_gain;
    float m_sampleRate;
    float m_delayTime, m_maxDelayTime;
    int m_delayTimeSamples;
    int m_bufferIndex = 0;
};

#endif // COMBFILTER_H_
