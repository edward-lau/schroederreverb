/*
  ==============================================================================

    AllPassFilter.cpp
    Created: 7 Dec 2017 2:33:48pm
    Author:  Edward Lau

  ==============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "AllPassFilter.h"

AllPassFilter::AllPassFilter ()
{
    m_buffer = AudioSampleBuffer (1, 441);
    m_buffer.clear();
    m_pBuffer = m_buffer.getWritePointer (0);
}

AllPassFilter::~AllPassFilter()
{
}

void AllPassFilter::initialize (float delayTime, float maxDelayTime, float gain)
{
    m_delayTime = delayTime;
    m_maxDelayTime = maxDelayTime;
    m_gain = gain;
}

void AllPassFilter::setSampleRate (double sampleRate)
{
    m_sampleRate = sampleRate;
    const int maxDelaySamples = (int)(m_maxDelayTime * sampleRate);
    if (maxDelaySamples != m_buffer.getNumSamples())
    {
        m_buffer.setSize (1, maxDelaySamples);
        m_buffer.clear();
        m_pBuffer = m_buffer.getWritePointer(0);
        m_bufferIndex = 0;
    }

    m_delayTimeSamples = (int)(m_delayTime * sampleRate);
}

float AllPassFilter::process (float sample)
{
    const float delayOutput = m_pBuffer[m_bufferIndex];
    m_pBuffer[m_bufferIndex] = sample + delayOutput * m_gain;
    m_bufferIndex = (m_bufferIndex != m_delayTimeSamples - 1 ? m_bufferIndex + 1 : 0);

    return delayOutput - m_gain * sample;
}